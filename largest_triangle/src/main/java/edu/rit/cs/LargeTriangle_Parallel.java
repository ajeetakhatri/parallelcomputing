/**
 * @author : Ajeeta Khatri
 * <p>
 * This program calculates the maximum area of a traingles from the given list of random numbers,
 * using cluster programming, where it runs on different nodes.
 * <p>
 * The program can be run using  mpirun java -cp ~/mpi.jar  edu.rit.cs.LargeTriangle_Parallel 100 100 142857
 */
package edu.rit.cs;
import mpi.MPI;
import mpi.MPIException;

public class LargeTriangle_Parallel {

    public static void main(String[] args) throws MPIException {

        long start = System.currentTimeMillis();

        Point p;
        int point_index = 0;
        int numPoints = Integer.parseInt(args[0]);
        Point[] points = new Point[numPoints];
        RandomPoints rndPoints = new RandomPoints(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));

        while (rndPoints.hasNext()) {
            p = rndPoints.next();
            points[point_index] = p;
            point_index++;
        }

        double ans = 0;
        int firstPoint = numPoints, secondPoint = numPoints, thirdPoint = numPoints;

        //Parallel code starts here
        MPI.Init(args);

        int rank = MPI.COMM_WORLD.getRank();
        int tasks = MPI.COMM_WORLD.getSize();
        int blockSize = numPoints / tasks;
        int[] sendBuff = new int[numPoints / blockSize];
        int[] recvbuf = new int[1];

        for (int i = 0; i < tasks; i++) {
            sendBuff[i] = numPoints - (i * blockSize);
        }

        MPI.COMM_WORLD.scatter(sendBuff, 1, MPI.INT, recvbuf, 1, MPI.INT, 0);

        int rem = recvbuf[0] % blockSize;
        int i = recvbuf[0] - blockSize == rem ? (blockSize + rem) - 1 : recvbuf[0] - 1;
        int lowerBound = recvbuf[0] - blockSize == rem ? 0 : recvbuf[0] - blockSize;
        while (i >= lowerBound) {
            for (int j = i - 1; j > 0; j--) {
                for (int k = j - 1; k > 0; k--) {
                    double ans_new = area(points[i], points[j], points[k]);
                    if (ans_new >= ans) {
                        ans = ans_new;
                        firstPoint = k;
                        secondPoint = j;
                        thirdPoint = i;
                    }
                }
            }
            i--;
        }

        MPI.Finalize();

        long end = System.currentTimeMillis();

        if (rank == 0) {
            System.out.println("Takes " + (end - start) / 1000F + " seconds");
            System.out.println(ans + "\n" + (firstPoint + 1) + " " + points[firstPoint].getX() + "," + points[firstPoint].getY());
            System.out.println((secondPoint + 1) + " " + points[secondPoint].getX() + "," + points[secondPoint].getY());
            System.out.println((thirdPoint + 1) + " " + points[thirdPoint].getX() + "," + points[thirdPoint].getY());
        }
    }

    /***
     This function calculates the area based on the given 3 cordinates.
     */
    public static double area(Point P, Point Q, Point R) {
        double a = Math.sqrt((Q.getY() - P.getY()) * (Q.getY() - P.getY()) + (Q.getX() - P.getX()) * (Q.getX() - P.getX())); //p q
        double b = Math.sqrt((R.getY() - Q.getY()) * (R.getY() - Q.getY()) + (R.getX() - Q.getX()) * (R.getX() - Q.getX())); // q r
        double c = Math.sqrt((P.getY() - R.getY()) * (P.getY() - R.getY()) + (P.getX() - R.getX()) * (P.getX() - R.getX())); // r p
        double s = (a + b + c) / 2;
        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }
}
