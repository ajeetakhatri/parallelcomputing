/*

@author: Ajeeta Khatri
Course: Parallel Computing
Assignment III

This is the code to compute modular cube root(s) in parallel on the GPU using a brute force search.
*/
#include <iostream>
#include <iomanip>
#include <math.h>
#include <sstream>
#include <sys/time.h>
#include <curand_kernel.h>
#include <stdio.h>
#include <ctime>
#include <ratio>
#include <chrono>

// Accessible by ALL CPU and GPU functions !!!
__managed__ int array_of_ms[100];
__managed__ int index_of_array = 0;
// Kernel function
__global__
void countPoints(int c, long long int n)
{
    // index = block index * number of threads per block + thread index
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    // stride  = number threads per block * number of block per grid
    int stride = blockDim.x * gridDim.x;

    for (long long int i = index; i < n; i += stride) {
        if (c ==  (((i * i) % n) * i % n) ){
            //array_of_ms[index_of_array++] = i;
            atomicAdd(&array_of_ms[index_of_array++], i);
        }        
    }
}
//sorting code
void sort(int a[], int n) {
  int i, j, min, temp;
  for (i = 0; i < n - 1; i++) {
     min = i;
     for (j = i + 1; j < n; j++)
        if (a[j] < a[min])
           min = j;
     temp = a[i];
     a[i] = a[min];
     a[min] = temp;
  }
}

// host code
int main(int argc, char* argv[]) {
  auto started = std::chrono::high_resolution_clock::now();
  // Check the number of parameters
  if (argc < 3) {
      // Tell the user how to run the program
      std::cerr << "Usage: " << argv[0] << " c n" << std::endl;
      return 1;
  }

  int c = std::stoi(argv[1]);
  int n = std::stoi(argv[2]);
  // Run kernel on some number of points on the GPU
  int blockSize = 256;
  int numBlocks = (c + blockSize - 1) / blockSize;
  countPoints<<<numBlocks, blockSize>>>(c, n);

  // Wait for GPU to finish before accessing on host
  cudaDeviceSynchronize();

  if (index_of_array == 0){
    std::cout << "No cube roots of " << c << "(mod "<< n << ")" << std::endl;  
  }
  int i=0;
  
  sort(array_of_ms, index_of_array);
  for (i = 0; i < index_of_array; i++) {
    std::cout << array_of_ms[i] << "^3 = " << c << "(mod "<< n << ")" << std::endl;
  }
  auto done = std::chrono::high_resolution_clock::now();
  std::cout <<"Takes "<< std::chrono::duration_cast<std::chrono::milliseconds>(done-started).count() << "ms"<< std::endl;
  
  return 0;
}
