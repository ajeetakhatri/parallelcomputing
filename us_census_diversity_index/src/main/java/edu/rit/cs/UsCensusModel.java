package edu.rit.cs;

public class UsCensusModel {
    private String stName;
    private String ctyName;
    private int year;
    private int agegrp;
    private int wa_Male;
    private int wa_Female;
    private int ba_Male;
    private int ba_Female;
    private int ia_Male;
    private int ia_Female;
    private int aa_Male;
    private int aa_Female;
    private int na_Male;
    private int na_Female;
    private int tom_male;
    private int tom_Female;

    public UsCensusModel(String reviewLine) {
        String rejex = String.format(",");
        String[] data = reviewLine.split(rejex, -1);

        try {
            this.stName = data[3];
            this.ctyName = data[4];
            this.year = Integer.valueOf(data[5]);
            this.agegrp = Integer.valueOf(data[6]);
            this.wa_Male = Integer.valueOf(data[10]);
            this.wa_Female = Integer.valueOf(data[11]);
            this.ba_Male = Integer.valueOf(data[12]);
            this.ba_Female = Integer.valueOf(data[13]);
            this.ia_Male = Integer.valueOf(data[14]);
            this.ia_Female = Integer.valueOf(data[15]);
            this.aa_Male = Integer.valueOf(data[16]);
            this.aa_Female = Integer.valueOf(data[17]);
            this.na_Male = Integer.valueOf(data[18]);
            this.na_Female = Integer.valueOf(data[19]);
            this.tom_male = Integer.valueOf(data[20]);
            this.tom_Female = Integer.valueOf(data[21]);
        } catch (Exception e) {
            System.out.println("In exception");
            System.err.println(e.toString());
        }
    }

    public String getStName() {
        return stName;
    }

    public void setStName(String stName) {
        this.stName = stName;
    }

    public String getCtyName() {
        return ctyName;
    }

    public void setCtyName(String ctyName) {
        this.ctyName = ctyName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getAgegrp() {
        return agegrp;
    }

    public void setAgegrp(int agegrp) {
        this.agegrp = agegrp;
    }

    public int getWa_Male() {
        return wa_Male;
    }

    public void setWa_Male(int wa_Male) {
        this.wa_Male = wa_Male;
    }

    public int getWa_Female() {
        return wa_Female;
    }

    public void setWa_Female(int wa_Female) {
        this.wa_Female = wa_Female;
    }

    public int getBa_Male() {
        return ba_Male;
    }

    public void setBa_Male(int ba_Male) {
        this.ba_Male = ba_Male;
    }

    public int getBa_Female() {
        return ba_Female;
    }

    public void setBa_Female(int ba_Female) {
        this.ba_Female = ba_Female;
    }

    public int getIa_Male() {
        return ia_Male;
    }

    public void setIa_Male(int ia_Male) {
        this.ia_Male = ia_Male;
    }

    public int getIa_Female() {
        return ia_Female;
    }

    public void setIa_Female(int ia_Female) {
        this.ia_Female = ia_Female;
    }

    public int getAa_Male() {
        return aa_Male;
    }

    public void setAa_Male(int aa_Male) {
        this.aa_Male = aa_Male;
    }

    public int getAa_Female() {
        return aa_Female;
    }

    public void setAa_Female(int aa_Female) {
        this.aa_Female = aa_Female;
    }

    public int getNa_Male() {
        return na_Male;
    }

    public void setNa_Male(int na_Male) {
        this.na_Male = na_Male;
    }

    public int getNa_Female() {
        return na_Female;
    }

    public void setNa_Female(int na_Female) {
        this.na_Female = na_Female;
    }

    public int getTom_male() {
        return tom_male;
    }

    public void setTom_male(int tom_male) {
        this.tom_male = tom_male;
    }

    public int getTom_Female() {
        return tom_Female;
    }

    public void setTom_Female(int tom_Female) {
        this.tom_Female = tom_Female;
    }
}