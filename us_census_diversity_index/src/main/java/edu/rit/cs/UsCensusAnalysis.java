package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;

import java.io.File;
import java.math.BigInteger;
import java.util.List;

public class UsCensusAnalysis {

    private static final String OutputDirectory = "dataset/us_census_diversity_index";
    private static final String DatasetFile = "dataset/cc-est2017-alldata.csv";

    private static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    private static void calculateDiversityIndex(SparkSession spark, String agegrp, String[] listofStates) {
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv("us_census_diversity_index/dataset/cc-est2017-alldata.csv");

        ds = ds.withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType))
                .filter(ds.col("AGEGRP").$eq$eq$eq(0)).filter(ds.col("YEAR").$eq$eq$eq(1)).filter(ds.col("STNAME").isin(listofStates))
        ;

        // Encoders are created for Java beans
        Encoder<UsCensusModel> reviewEncoder = Encoders.bean(UsCensusModel.class);
        Dataset ds2 = ds.as(reviewEncoder);
        //ds1.show();

        //extract required column
//        Dataset ds2 = ds1.select(ds1.col("STNAME"), ds1.col("CTYNAME"),
//                ds1.col("YEAR"), ds1.col("AGEGRP"),
//                ds1.col("WA_MALE"), ds1.col("WA_FEMALE"),
//                ds1.col("BA_MALE"), ds1.col("BA_FEMALE"),
//                ds1.col("IA_MALE"), ds1.col("IA_FEMALE"),
//                ds1.col("AA_MALE"), ds1.col("AA_FEMALE"),
//                ds1.col("NA_MALE"), ds1.col("NA_FEMALE"),
//                ds1.col("TOM_MALE"), ds1.col("TOM_FEMALE"));
//        ds2.show();

        Dataset ds3 = ds2.withColumn("STNAME", ds2.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds2.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds2.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds2.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_TOTAL", ds2.col("WA_MALE").$plus(ds2.col("WA_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("BA_TOTAL", ds2.col("BA_MALE").$plus(ds2.col("BA_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("IA_TOTAL", ds2.col("IA_MALE").$plus(ds2.col("IA_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("AA_TOTAL", ds2.col("AA_MALE").$plus(ds2.col("AA_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("NA_TOTAL", ds2.col("NA_MALE").$plus(ds2.col("NA_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("TOM_TOTAL", ds2.col("TOM_MALE").$plus(ds2.col("TOM_FEMALE")).cast(DataTypes.IntegerType))
                .withColumn("TOTAL", ds2.col("WA_MALE").$plus(ds2.col("WA_FEMALE"))
                        .$plus(ds2.col("BA_MALE")).$plus(ds2.col("BA_FEMALE"))
                        .$plus(ds2.col("IA_MALE")).$plus(ds2.col("IA_FEMALE"))
                        .$plus(ds2.col("AA_MALE")).$plus(ds2.col("AA_FEMALE"))
                        .plus(ds2.col("NA_MALE")).$plus(ds2.col("NA_FEMALE"))
                        .$plus(ds2.col("TOM_MALE")).$plus(ds2.col("TOM_FEMALE")).cast(DataTypes.IntegerType));
        ds3.show();
        JavaRDD rdd1 = ds3.toJavaRDD();
        JavaRDD diversityIndexRdd = rdd1.map(new Function<Row, String>() {

            public String call(Row row) throws Exception {
                StringBuilder returnArray = new StringBuilder();
                String stateName = (String) row.get(0);
                String countyName = (String) row.get(1);
                Integer totalNumber = (Integer) row.get(22);
                Double densityIndex = calculateDensity(row, totalNumber);
                returnArray.append("[").append(stateName).append(",");
                returnArray.append(countyName).append(",");
                returnArray.append(densityIndex.toString()).append("]");
                return returnArray.toString();
            }

            private Double calculateDensity(Row row, Integer totalNumber) {
                double index;
                BigInteger onebyTsquare = BigInteger.valueOf(totalNumber).multiply(BigInteger.valueOf(totalNumber));
                BigInteger summation = BigInteger.valueOf(0);
                for (int i = 16; i < 22; i++) {
                    BigInteger subterm = BigInteger.valueOf(totalNumber).subtract(BigInteger.valueOf((Integer) row.get(i)));
                    BigInteger temp = BigInteger.valueOf((Integer) row.get(i)).multiply(subterm);
                    summation = summation.add(temp);
                }
                //index = (summation.remainder(onebyTsquare)).doubleValue();
                index = summation.doubleValue() / onebyTsquare.doubleValue();
                return (index);
            }
        });
        List whatyouWant = diversityIndexRdd.collect();
        JavaRDD finalRdd = diversityIndexRdd.repartition(4);
        deleteDirectory(new File("us_census_diversity_index/" + OutputDirectory + "_3"));
        finalRdd.saveAsTextFile("us_census_diversity_index/" + OutputDirectory + "_3");
    }


    public static void main(String[] args) throws Exception {
        // Create a SparkConf that loads defaults from system properties and the classpath
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("US Census");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
        String ageGrp = args[0];
        int totalCount = args.length;
        String listOfStates[] = new String[totalCount - 1];
        int i = 0;
        while (i < totalCount - 1) {
            listOfStates[i] = args[i + 1];
            i++;
        }
        calculateDiversityIndex(spark, ageGrp, listOfStates);

        // Stop existing spark context
        jsc.close();

        // Stop existing spark session
        spark.close();
    }
}