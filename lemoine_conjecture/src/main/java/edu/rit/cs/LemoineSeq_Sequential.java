package edu.rit.cs;

/**
 * @author ajeeta
 * This is the sequential version of the code to verify Lemoine's Conjecture, which states
 * Every odd integer greater than 5 is the sum of a prime and twice a prime.
 * <p>
 * In other words,
 * In other words, if n is odd and n > 5, then n = p + 2q for some primes p and q.
 *
 * The code can be run with this command
 * java -cp class edu.rit.cs.LemoineSeq_Sequential 1000001 1999999
 */
public class LemoineSeq_Sequential {

    public static void main(String[] args) {
        if (args.length > 2) {
            System.out.println("Incorrect number args");
            System.exit(0);
        }
        int lowerBound = Integer.parseInt(args[0]);
        int upperBound = Integer.parseInt(args[1]);
        if (isOdd(lowerBound) && isOdd(upperBound) && upperBound >= lowerBound) {
            verifyLemoineSquential(lowerBound, upperBound);
        } else {
            System.out.println("Invalid inputs");
        }
    }

    /**
     * This is the function which verifies Lemoine's conjecture in a sequential manner.
     * Where we start from the lowerbound and go upto upper bound,
     * on way checking all the numbers that satisfy the conjectue.
     * <p>
     * The approach which we follow here in order to fasten the process is start with
     * q = n/2 because as per the equation q can never be greater than n/2.
     * And then keep checking for all q, and find the corresponding minimum value of p for that number.
     *
     * @param lowerBound
     * @param upperBound
     */
    private static void verifyLemoineSquential(int lowerBound, int upperBound) {
        int maxp = 0;
        int finalN = 0;
        long start = System.currentTimeMillis();
        for (int n = lowerBound; n <= upperBound; n += 2) {
            for (int q = n / 2; q >= 2; q--) {
                int p = n - (2 * q);
                if (Prime.isPrime(p) && Prime.isPrime(q) && (n == p + 2 * q)) {
                    if (p >= maxp) {
                        maxp = p;
                        finalN = n;
                    }
                    break;
                }
            }
        }
        long end = System.currentTimeMillis();
        System.out.println("Takes " +
                (end - start) + "ms");
        System.out.println(finalN + " = " + maxp + " + 2 * " + (finalN - maxp) / 2);
    }

    /**
     * This function check if the number is odd.
     *
     * @param number
     * @return
     */
    private static boolean isOdd(int number) {
        return number % 2 == 1;
    }
}